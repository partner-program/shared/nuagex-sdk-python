from codecs import open as codecs_open
from setuptools import setup, find_packages


# Get the long description from the relevant file
with codecs_open('README.rst', encoding='utf-8') as f:
    long_description = f.read()


setup(name='nuagex',
      version='0.2.2',
      description=u"NuageX Python Client",
      long_description=long_description,
      classifiers=[],
      keywords='',
      author=u"Remi Vichery",
      author_email='remi.vichery@nuagenetworks.net',
      url='https://github.com/nuagex/python-nuagex',
      license='MIT',
      packages=find_packages(exclude=['ez_setup', 'examples', 'tests']),
      include_package_data=True,
      zip_safe=False,
      install_requires=[
          'requests'
      ],
      extras_require={
          'test': ['pytest'],
      }
      )
