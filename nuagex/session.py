# -*- coding: utf-8 -*-
#
# Copyright (c) 2015, Alcatel-Lucent Inc
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the copyright holder nor the names of its contributors
#       may be used to endorse or promote products derived from this software without
#       specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import json
import requests

from nuagex import error as exc

class Session(object):

    def __init__(self, api_url, username, password):
        self.api_url = api_url
        self.username = username
        self.password = password
        self.headers = {
            'Content-type': 'application/json'
        }

    def authenticate(self):
        method = 'POST'
        url = '/auth/login'
        data = {
            'username': self.username,
            'password': self.password
        }
        response = self._send_request(method, url, data)
        auth = json.loads(response.text)
        self.headers['Authorization'] = 'Bearer ' + auth['accessToken']

    def fetch(self, rest_object, expand=False):
        method = 'GET'
        url = rest_object.get_resource_path()
        params = {}
        if expand:
            params = {'expand': 'all'}
        try:
            response = self._send_request(method, url, params=params)
        except exc.NuageXException as e:
            raise Exception("Unable to fetch %s with id=%s: %s" % (rest_object.__class__.__name__, rest_object._id, e.message))
        rest_object.set_attributes(**(json.loads(response.text)))

    def delete(self, rest_object):
        method = 'DELETE'
        url = rest_object.get_resource_path()
        try:
            self._send_request(method, url)
        except exc.NuageXException as e:
            raise Exception("Unable to delete %s with id=%s: %s" % (rest_object.__class__.__name__, rest_object._id, e.message))

    def save(self, rest_object):
        method = 'POST'
        url = rest_object.get_resource_path()
        if rest_object._id is not None:
            method = 'PUT'
        try:
            response = self._send_request(method, url, data=rest_object.to_dict())
        except exc.NuageXException as e:
            if rest_object._id is not None:
                raise Exception("Unable to save %s with id=%s: %s" % (rest_object.__class__.__name__, rest_object._id, e.message))
            else:
                raise Exception("Unable to create %s with body=%s: %s" % (rest_object.__class__.__name__, rest_object.to_dict(), e.message))
        rest_object.set_attributes(**(json.loads(response.text)))

    def _send_request(self, method, url, data=None, params=None):
        complete_url = self.api_url + url
        response = None
        try:
            response = requests.request(method=method,
                                        url=complete_url,
                                        headers=self.headers,
                                        params=params,
                                        json=data)
        except Exception as e:
            raise exc.NuageXException('Failed to send request: %s' % e)

        if response.status_code == 401 or response.status_code == 403:
            self.authenticate()
            try:
                response = requests.request(method=method,
                                            url=complete_url,
                                            headers=self.headers,
                                            params=params,
                                            json=data)
            except Exception as e:
                raise exc.NuageXException('Failed to send request: %s' % e.message)

        if not 200 <= response.status_code < 300:
            if response.status_code == 400:
                raise exc.InvalidRequest(json.loads(response.text)['message'], response, response.status_code)
            elif response.status_code == 403:
                raise exc.InsufficientPermissions(response, response.status_code)
            elif response.status_code == 404:
                raise exc.NotFound(response, response.status_code)
            else:
                raise exc.NuageXException(json.loads(response.text)['message'], response, response.status_code)
        return response
